import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { GetListService } from '../../get-list.service';
import { FoodDishService } from '../food-dish.service';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFirePerformance } from '@angular/fire/performance';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { StorageService } from 'src/app/shared/services/storage.service';

export interface Lists {
  description: String;
  id: String;
  imagePath: String;
  imageURL: String;
  name: String;
  restaurantId: String;
}
@Component({
  selector: 'app-list-food-dish',
  templateUrl: './list-food-dish.component.html',
  styleUrls: ['./list-food-dish.component.css']
})
export class ListFoodDishComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  public pageLength;
  public dataSource = new MatTableDataSource<Lists>();
  restaurant = [];
  categoryList = [];
  showLoader = true;
  selectedRestaurant = '';
  typeCategory = '';
  restaurantId;
  page = 0;
  size = 8;
  modalDetails: any;
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  listForm: FormGroup;
  constructor(
    private Service: FoodDishService,
    private formBuilder: FormBuilder,
    private Route: Router,
    private List: GetListService,
    private storage: StorageService,
    private modalService: NgbModal,
    private preformance: AngularFirePerformance) { }

  ngOnInit(): void {
    this.restaurantList();
    this.dataSource.paginator = this.paginator;
    this.listForm = this.formBuilder.group({
      food_category_Search: [null, Validators.required],
      restaurant_Id: [null, Validators.required],
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {

    });
  }
  formatterConstitution(result: any) {
    return result.name;
  }
  selectId(event) {
    console.log(event);
  }
  searchConstituon = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusConstitution$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

 restaurantList() {
  const query = {
    'searchText': ''
  };
  console.log('restaurant Name api query', query);
  this.List.restaurantNameList(query)
    .then(
      (res: any) => {
        console.log(res, 'restaurant Name api response');
        this.restaurant = res['data'];
        console.log(this.restaurant , 'restaurant list');
      },
      (err: Object) => {
        console.log(err, 'restaurant Name api error');
      }
    )
    .catch((err: Object) => {
      console.log(err, 'restaurant Name error catch');
  });

  }

  selectedRestaurantId(data) {
    this.restaurantId = data.item['id'];
    console.log('restaurant selected', data);
    this.listFoodDish(this.restaurantId);
  }

  createDish() {
    this.Route.navigate(['/food-dish/create-dish']);
  }
  edit() {
    this.storage.setItem('foodDetails', this.modalDetails);
    this.Route.navigate(['/food-dish/create-dish',  {edit: true}]);
  }
  searchCategory() {
    if (this.restaurantId) {
      this.listFoodDish(this.restaurantId);
    }
  }
  onPaginate(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;

    // this.page = this.page + 1;
    this.listFoodDish(this.restaurantId);
  }
  listFoodDish(id) {
    const query = {
      'restaurantId': id,
      'searchText': this.typeCategory,
      'page': this.page,
      'size': this.size,
      'sortBy': 'name',
      'orderBy': 'ASC'
    };
    console.log('Food dish api query', query);
    this.Service.listFoodDish(query)
      .then(
        (res: any) => {
          console.log(res, 'Food dish api response');
          this.dataSource.data  = res['data'][0].foodDishList as Lists[];
          this.pageLength = res['data'][0].totalPages * this.size;
          setTimeout(() => {
            this.showLoader = false;
          }, 2000);
          console.log(this.categoryList, 'category List');
        },
        (err: Object) => {
          console.log(err, 'Food dish  api error');
          this.showLoader = false;
        }
      )
      .catch((err: Object) => {
        console.log(err, 'restaurant Name error catch');
    });

  }

  foodDetails(content, values) {
    this.modalDetails = values;
    console.log('values', this.modalDetails);
    this.openModal(content);
  }
  openModal(content) {
    this.modalService.open(content, {
    backdrop: true,
    centered: true,
    keyboard: true,
    });
}
}

