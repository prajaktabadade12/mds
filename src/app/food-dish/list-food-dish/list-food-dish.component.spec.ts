import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFoodDishComponent } from './list-food-dish.component';

describe('ListFoodDishComponent', () => {
  let component: ListFoodDishComponent;
  let fixture: ComponentFixture<ListFoodDishComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListFoodDishComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFoodDishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
