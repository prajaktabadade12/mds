import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFoodDishComponent } from './create-food-dish.component';

describe('CreateFoodDishComponent', () => {
  let component: CreateFoodDishComponent;
  let fixture: ComponentFixture<CreateFoodDishComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateFoodDishComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFoodDishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
