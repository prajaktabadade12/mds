import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { FoodDishService } from '../food-dish.service';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { GetListService } from '../../get-list.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirePerformance } from '@angular/fire/performance';
import { StorageService } from '../../shared/services/storage.service';

@Component({
  selector: 'app-create-food-dish',
  templateUrl: './create-food-dish.component.html',
  styleUrls: ['./create-food-dish.component.css']
})
export class CreateFoodDishComponent implements OnInit {
  filepath;
  downloadableURL = '';
  isAvailableForSale = false;
  task: AngularFireUploadTask;
  public model: any;
  progressValue: Observable<number>;
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  focusCategory$   = new Subject<string>();
  clickCategory$ =  new Subject<string>();
  restaurant = [];
  category = [];
  filename;
  editDetails = false;
  title = 'Create';
  foodDetails: any;
  register: FormGroup;
  uuid: any;
  editData: any = {
  available: '',
  foodPriceWithDiscount: '',
  foodPriceWithoutTax: '',
  description: '',
  foodCategory_id: '',
  foodCategoryName: '',
  foodType: 'VEG',
  imageURL: '',
  name: '',
  restaurantId: ''
  };
  restaurantSelected = '';
  categorySelected = '';
  constructor(private formBuilder: FormBuilder,
    private Service: FoodDishService,
    public notify: NotificationService,
    private Route: Router,
    private List: GetListService,
    private angularFire: AngularFireStorage,
    private activatedRoute: ActivatedRoute,
    private storage: StorageService,
    private performance: AngularFirePerformance) {
    if (activatedRoute.snapshot.params.edit !== undefined) {
      this.editDetails =  activatedRoute.snapshot.params.edit;
      this.foodDetails = this.storage.getItem('foodDetails');
      // console.log(this.foodDetails, 'food details');
      this.title = 'Edit';
      this.isAvailableForSale = this.foodDetails.available;
      this.downloadableURL = this.foodDetails.imageURL;
      this.editData = {
        available: this.foodDetails.available,
        foodPriceWithDiscount: this.foodDetails.foodPriceWithDiscount,
        foodPriceWithoutTax: this.foodDetails.foodPriceWithoutTax,
        description: this.foodDetails.description,
        foodCategory_id: this.foodDetails.foodCategory.id,
        foodCategoryName: this.foodDetails.foodCategory.name,
        foodType: this.foodDetails.foodType,
        name: this.foodDetails.name,
        restaurantId: this.foodDetails.restaurantId,
      };
    } else {
        this.editDetails = false;
    }
      // console.log(this.storage.getItem('userDetails'));
      this.uuid = this.storage.getItem('userDetails');
     }

  ngOnInit(): void {
    this.restaurantList();
    // this.foodCategoryList();
    // console.log('food dish');
    this.register = this.formBuilder.group({
      food_dish_name: [this.editData.name, Validators.required],
      food_dish_description: [this.editData.description],
      restaurant_name: [null, Validators.required],
      food_type: [this.editData.foodType,  Validators.required],
      food_category: [null, Validators.required],
      pricewithout_tx: [this.editData.foodPriceWithoutTax, Validators.required],
      price_discount:  [this.editData.foodPriceWithDiscount, Validators.required],
    });
  }

  async imageUpload(file) {
    const fileData = file.target.files[0];
    this.filename = file.target.files[0].name;
    // console.log('file', fileData);
    if (fileData) {
      this.filepath =  Math.random() + fileData;
      this.performance.trace('Food dish imge upload performance').then(success => {
        // console.log('success', success);
      });
      this.task = this.angularFire.upload(`restaurant/food-dish/${this.filepath}`, fileData);
      this.progressValue = this.task.percentageChanges();
      (await this.task).ref.getDownloadURL().then(url => {this.downloadableURL = url; });
      // console.log(this.filepath, 'filepath');
    } else {
      alert('No images selected');
      this.downloadableURL = '';
    }
  }

  formatterConstitution(result: any) {
    return result.name;
  }
  selectId(event) {
    // console.log(event);
    const id = event.item.id;
    this.foodCategoryList(id);
  }
  searchConstituon = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusConstitution$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }


  formatterCategory(result: any) {
    return result.name;
  }

  searchCategory = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusCategory$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.category .filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  CreateFoodDish() {
   const query = {
    'name': this.register.value.food_dish_name,
    'description': this.register.value.food_dish_description,
    'imageURL': `${this.downloadableURL}`,
    'imagePath': `restaurant/food-categories/${this.filepath}`,
    'restaurantId': this.register.value.restaurant_name ? this.register.value.restaurant_name.id : '',
    'foodPriceWithoutTax': this.register.value.pricewithout_tx,
    'foodPriceWithDiscount':  this.register.value.price_discount,
    'foodType': this.register.value.food_type,
    'foodCategoryId': this.register.value.food_category.id,
    'available': this.isAvailableForSale,
    'createdBy': this.uuid.id
   };
  //  console.log('create food dish api query', query);
   this.Service.createFoodFish(query)
     .then(
      (res: any) => {
        // console.log(res, 'create food dish api response');
        this.notify.showSuccess(`Food Dish created`, 'Succesfully Created');
        setTimeout(() => {
        this.Route.navigate(['/food-dish']);
        }, 1000);
      },
      (err: Object) => {
        // console.log(err, 'create food dish api error');
        this.notify.showError(err['error'].errorResponse.description,'Error !!!');
        this.Route.navigate([this.Route.url]);
      }
     )
     .catch((err: Object) => {
      //  console.log(err, 'create food dish error catch');
    });
  }


 restaurantList() {
  const query = {
    'searchText': ''
  };
  // console.log('restaurant Name api query', query);
  this.List.restaurantNameList(query)
    .then(
      (res: any) => {
        // console.log(res, 'restaurant Name api response');
        this.restaurant = res['data'];
        // console.log(this.restaurant , 'restaurant list');
        this.restaurant.map((item) => {
          // tslint:disable-next-line:triple-equals
          // console.log(this.editData.restaurantId);
          // tslint:disable-next-line:triple-equals
          if (item.id == this.editData.restaurantId) {
              // console.log(item.name, 'resto !!!!!!!!!!!!!!!!!!!!!!!!!!');
              this.foodCategoryList(this.editData.restaurantId);
              this.register.patchValue({
                restaurant_Id: item
              });
              this.restaurantSelected = item.name;
          }
        });
      },
      (err: Object) => {
        // console.log(err, 'restaurant Name api error');
      }
    )
    .catch((err: Object) => {
      // console.log(err, 'restaurant Name error catch');
    });
  }

  categoryList() {
    const query = {
      'searchText': ''
    };
    // console.log('restaurant Name api query', query);
    this.List.restaurantNameList(query)
      .then(
        (res: any) => {
          // console.log(res, 'restaurant Name api response');
          this.restaurant = res['data'];
          // console.log(this.restaurant , 'restaurant list');
        },
        (err: Object) => {
          // console.log(err, 'restaurant Name api error');
        }
      )
      .catch((err: Object) => {
        // console.log(err, 'restaurant Name error catch');
    });
  }

  foodCategoryList(id) {
    const query = {
      'restaurantId':  id,
      'searchText': '',
      'page': 0,
      'size': 1000000,
      'sortBy': 'name',
      'orderBy': 'ASC'
    };
    // console.log('food category api query', query);
    this.Service.listFoodCategory(query)
      .then(
        (res: any) => {
          // console.log(res, 'food category api response');
          this.category = res['data'][0].categoryList;
          // console.log(this.category, 'category List');
          this.category.map((item) => {
            // tslint:disable-next-line:triple-equals
            // console.log(this.editData.foodCategory_id, 'category id');
            // tslint:disable-next-line:triple-equals
            if (item.id == this.editData.foodCategory_id) {
                // console.log(item.name, 'category !!!!!!!!!!!!!!!!!!!!!!!!!!');
                this.register.patchValue({
                  food_category: item
                });
                this.categorySelected = item.name;
            }
          });
        },
        (err: Object) => {
          // console.log(err, 'food category api error');
        }
      )
      .catch((err: Object) => {
        // console.log(err, 'restaurant Name error catch');
    });
  }
}
