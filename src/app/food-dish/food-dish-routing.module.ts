import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFoodDishComponent } from './list-food-dish/list-food-dish.component';
import { CreateFoodDishComponent } from './create-food-dish/create-food-dish.component';
export const FoodRoutes: Routes = [
  {
    path: 'create-dish',
    children: [{
      path: '',
      component: CreateFoodDishComponent
  }
]},
  {
    path: '',
    children: [{
      path: '',
      component: ListFoodDishComponent
  }
  ]}
];
