import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { GetListService } from '../../get-list.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PromoService } from '../promo.service';
import { StorageService } from '../../shared/services/storage.service';
interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: EventTarget;
  getMessage(): string;
}
@Component({
  selector: 'app-create-promo-code',
  templateUrl: './create-promo-code.component.html',
  styleUrls: ['./create-promo-code.component.css']
})
export class CreatePromoCodeComponent implements OnInit {
  public model: any;
  progressValue: Observable<number>;
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  restaurant = [];
  filename;
  uuid: any;
  restaurantSelected = '';
  title = 'Create Promo Code';
  editDetails = false;
  codeTetails: any;
  register: FormGroup;
  editData = {
    description: '',
    discount: '',
    id: '',
    maximumDiscountValue: '',
    maximumUsageCountPerUser: '',
    minimumCartValue: '',
    name: '',
    restaurantId: '',
    validityDate: ''
  };
  constructor(private formBuilder: FormBuilder,
    public notify: NotificationService,
    private Route: Router,
    private Service: PromoService,
    private activatedRoute: ActivatedRoute,
    private storage: StorageService,
    private List: GetListService) {
      if (activatedRoute.snapshot.params.edit !== undefined) {
        this.editDetails =  activatedRoute.snapshot.params.edit;
        this.codeTetails = this.storage.getItem('editPromoCode');
        // console.log(this.codeTetails, 'food details');
        this.title = 'Edit Promo Code';
        this.editData = {
          description: this.codeTetails.description,
          discount: this.codeTetails.discount,
          id: this.codeTetails.id,
          maximumDiscountValue: this.codeTetails.maximumDiscountValue,
          maximumUsageCountPerUser: this.codeTetails.maximumUsageCountPerUser,
          minimumCartValue: this.codeTetails.minimumCartValue,
          name: this.codeTetails.name,
          restaurantId: this.codeTetails.restaurantId,
          validityDate: this.codeTetails.validityDate,
        };
      } else {
          this.editDetails = false;
      }
      // console.log(this.storage.getItem('userDetails'));
      this.uuid = this.storage.getItem('userDetails');
    }

  ngOnInit(): void {
    this.restaurantList();
    this.register = this.formBuilder.group({
      promo_code_name: [this.editData.name, Validators.required],
      promo_code_description: [this.editData.description],
      discount: [this.editData.discount, Validators.required],
      maximum_discount_value: [this.editData.maximumDiscountValue, Validators.required],
      maximum_usage_count: [this.editData.maximumUsageCountPerUser, Validators.required],
      validity_date: [this.editData.validityDate, Validators.required],
      restaurant_id: [this.editData.restaurantId, Validators.required],
      minimum_cart_value: [this.editData.minimumCartValue, Validators.required],
    });
    if (this.editDetails) {
      this.register.patchValue({
        validity_date: this.editData.validityDate
      });
    }
  }


  formatterConstitution(result: any) {
    return result.name;
  }
  selectId(event) {
    // console.log(event);
  }
  searchConstituon = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusConstitution$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  restaurantList() {
    const query = {
      'searchText': ''
    };
    // console.log('restaurant Name api query', query);
    this.List.restaurantNameList(query)
      .then(
        (res: any) => {
          // console.log(res, 'restaurant Name api response');
          this.restaurant = res['data'];
          // console.log(this.restaurant , 'restaurant list');
          this.restaurant.map((item) => {
            // tslint:disable-next-line:triple-equals
            // console.log(this.editData.restaurantId);
            // tslint:disable-next-line:triple-equals
            if (item.id == this.editData.restaurantId) {
                // console.log(item.name, 'resto !!!!!!!!!!!!!!!!!!!!!!!!!!');
                this.restaurantSelected = item.name;
            }
          });
        },
        (err: Object) => {
          // console.log(err, 'restaurant Name api error');
        }
      )
      .catch((err: Object) => {
        // console.log(err, 'restaurant Name error catch');
    });
  }

  CreatePromoCode() {
    // console.log(this.register, 'register form');
    const query = {
      'id': '',
      'name': this.register.value.promo_code_name,
      'description': this.register.value.promo_code_description,
      'discount': this.register.value.discount,
      'minimumCartValue': this.register.value.minimum_cart_value,
      'maximumDiscountValue': this.register.value.maximum_discount_value,
      'maximumUsageCountPerUser': this.register.value.maximum_usage_count,
      'validityDate': this.register.value.validity_date,
      'createdBy': this.uuid.id,
      'restaurantId': this.register.value.restaurant_id ? this.register.value.restaurant_id.id : ''
    };
    // console.log('create  promo code api query', query);
    this.Service.createPromoCode(query)
      .then(
       (res: any) => {
        //  console.log(res, 'create promo code api response');
         this.notify.showSuccess(`Promo Code has been created`, 'Succesfully Created');
         setTimeout(() => {
         this.Route.navigate(['/promo-code']);
         }, 1000);
       },
       (err: Object) => {
        //  console.log(err, 'create  promo code api error');
        this.notify.showError(err['error'].errorResponse.description,'Error !!!');
        this.Route.navigate([this.Route.url]);
       }
      )
      .catch((err: Object) => {
        // console.log(err, 'create promo code error catch');
    });
  }

}
