import { Injectable } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { URLConstants } from '../urlConstants';

@Injectable({
  providedIn: 'root'
})
export class PromoService {

  constructor(private http: HttpService) { }

  createPromoCode(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.createPromoCode, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  listPromoCode(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getPromoCodeList, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
