import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoCodeRoutes } from './promo-code-routing.module';
import { CreatePromoCodeComponent } from './create-promo-code/create-promo-code.component';
import { ListPromoCodeComponent } from './list-promo-code/list-promo-code.component';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { MaterialModule } from '../app.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CreatePromoCodeComponent, ListPromoCodeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(PromoCodeRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    NgbModule
  ]
})
export class PromoCodeModule { }
