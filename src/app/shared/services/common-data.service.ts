import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonDataService {

  constructor() { }

  countryCodeData() {
    return [{code: '+1'}, {code: '+233'}, {code: '+420'}, {code: '+91'}];
  }
  adminRoleData() {
    return [{role: 'ADMIN'}, {role: 'MANAGER'}, {role: 'CASHIER'},
    {role: 'KITCHEN'}, {role: 'DISPATCH'}, {role: 'SUPPORT'}, {role: 'CUSTOMER'}];
  }
  loginRoleData() {
    return [{role: 'ADMIN'}, {role: 'MANAGER'}, {role: 'CASHIER'}, {role: 'SUPPORT'}, {role: 'GLOBAL ADMIN'}];
  }
  userRoleData() {
    return [{role: 'ADMIN'}, {role: 'MANAGER'}, {role: 'CASHIER'},
    {role: 'KITCHEN'}, {role: 'DISPATCH'}, {role: 'SUPPORT'}, {role: 'GLOBAL ADMIN'}];
  }
}
