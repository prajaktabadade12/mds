import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { URLConstants } from '../../urlConstants';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public api_url = URLConstants.mainUrl;
  constructor(private http: HttpClient) { }
  call(data, api, method) {
    const headers = new HttpHeaders();
    return this.http.request(method, this.api_url + api, {
      body: data,
      headers: headers
    });
  }
}
