import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { StorageService } from './storage.service';
import { URLConstants } from '../../urlConstants';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { NotificationService } from './notification.service';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {
  public jwtToken;
  public urlsToNotUse;
  public firstApiToSkip = URLConstants.mainUrl + URLConstants.getlogin;
  public secondApiToSkip = URLConstants.mainUrl + URLConstants.resendOtp;
  public thisrdApiToSkip = URLConstants.mainUrl + URLConstants.submitOtp;
  constructor(private storage: StorageService,
    private notify: NotificationService,
    private router: Router) {
    console.log('jwt token', this.jwtToken);
    this.urlsToNotUse = [
      this.firstApiToSkip,
      this.secondApiToSkip,
      this.thisrdApiToSkip
    ];
  }



  intercept(request , execute) {
    if (this.isValidRequestForInterceptor(request.url)) {
      this.jwtToken  = this.storage.getItem('jwtToken');
      const tokenrequest = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.jwtToken}`
        }
      });
      // console.log('intercepted !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
      return execute.handle(tokenrequest)
      .pipe(catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.notify.showError('Unauthorized user please login !', `${error.status}`);
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 2000);
        }
        return throwError(error);
      }));
    } else {
      // console.log('not intercepted !!!!!!!!!!!!!!!!!!!!!!');
    return execute.handle(request);
    }
  }

  isValidRequestForInterceptor(requestUrl): boolean {
      const destination: string = requestUrl;
      for (const address of this.urlsToNotUse) {
        if (new RegExp(address).test(destination)) {
          return false;
        }
      }
    return true;
  }
}
