import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    MapRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule
    .forRoot({
      apiKey: 'AIzaSyA2ileB2soUkKvVWgZxtEaRM4Ws21ctvdo',
      libraries: ['places']
    }),
    // GooglePlaceModule,
  ],
  exports: [MapComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MapModule { }
