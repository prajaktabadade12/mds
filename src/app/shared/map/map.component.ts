/// <reference types="@types/googlemaps" />
import { Component, OnInit, EventEmitter, Output, Input, OnChanges, ViewChild, ElementRef, NgZone } from '@angular/core';
import { AgmGeocoder, AgmMap, AgmMarker, MapsAPILoader } from '@agm/core';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ComponentRestrictions } from 'ngx-google-places-autocomplete/objects/options/componentRestrictions';

declare let google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {
  latitude =  51.678418;
  longitude = 7.809007;
  zoom = 16;
  address;
  locationSelected = false;
  isSubmitted;
  addressString = [];
  autocompleteInput: string;
  queryWait: boolean;
  formattedAddress = '';
  @Output() location = new EventEmitter;
  @Input() submit: any;
  // @ViewChild('placesRef') placesRef: GooglePlaceDirective;
  @ViewChild('search') searchElementRef: ElementRef;
  add = [{latitude: this.latitude, longitude: this.longitude , address: this.addressString }];
  options = {
    ComponentRestrictions: {
      country : ['IND']
    }
  };
  zip_code: string;
  name: string;
  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  ngOnInit(): void {
    if (!navigator.geolocation) {
      console.log('location is not supported on this device');
    } else {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log(`lat: ${position.coords.latitude}  lon: ${position.coords.longitude}`);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.add = [{latitude: this.latitude, longitude: this.longitude , address: this.addressString }];
      }, (err) => {
        console.log(err);
      }, {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      });
    }
  }
  ngOnChanges(change) {
    this.isSubmitted = change.submit.currentValue;
    console.log(this.isSubmitted, 'change');

    if (this.isSubmitted) {
      this.getAddress(this.latitude, this.longitude);
      this.findAdress();
    }
  }
  chooseLocation(location) {
    this.latitude = location.coords.lat;
    this.longitude = location.coords.lng;
    this.locationSelected = true;
    // this.getAddress(this.latitude, this.longitude);
  }

  mapReady() {
    this.findAdress();
    if (this.isSubmitted) {
      this.getAddress(this.latitude, this.longitude);
    }
  }

  getAddress(lat, lng) {
    console.log('Finding Address ...');
    if (navigator.geolocation) {
      console.log('getting into loactions');
      const geocoder = new google.maps.Geocoder();
      const latlng = new google.maps.LatLng(lat, lng);
      const request = { latLng: latlng };
      geocoder.geocode(request, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        const result = results[0];
        this.addressString = result.address_components;
        this.add = [{latitude: this.latitude, longitude: this.longitude , address: this.addressString }];
        setTimeout(() => {
          this.location.emit(this.add);
        });
        }
      });
    }
  }

  findAdress() {
    this.locationSelected = true;
    this.mapsAPILoader.load().then(() => {
         const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
         autocomplete.addListener('place_changed', () => {
           this.ngZone.run(() => {
             // some details
             const place: google.maps.places.PlaceResult = autocomplete.getPlace();
             this.address = place.formatted_address;
             console.log(' this.address',  this.address);
            //  this.web_site = place.website;
             this.name = place.name;
             this.zip_code = place.address_components[place.address_components.length - 1].long_name;
             // set latitude, longitude and zoom
             this.latitude = place.geometry.location.lat();
             this.longitude = place.geometry.location.lng();
             this.zoom = 12;
           });
         });
       });
   }
}
