import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { StorageService } from '../services/storage.service';
import { MastersService } from '../../masters.service';

@Component({
  selector: 'app-user-details-modal',
  templateUrl: './user-details-modal.component.html',
  styleUrls: ['./user-details-modal.component.css']
})
export class UserDetailsModalComponent implements OnInit, OnChanges {
  @Input() title  = '';
  @Input() data: any;
  @Input() icon = '';
  details;
  editData;
  dataLoaded = false;
  constructor(public active: NgbActiveModal,
    private Route: Router,
    private storage: StorageService,
    private Detail: MastersService) { }

  ngOnInit(): void {
    console.log('this.title', this.title);
    console.log('data', this.data);
    this.userDetails(this.data.id);
  }

  ngOnChanges(change) {
    console.log('change', change);
  }

  edit() {
    this.storage.setItem('editUserData', this.editData);
    this.Route.navigate(['/create/user', {edit: true}]);
    this.active.dismiss('cross clicked');
  }

  userDetails(id) {
    const query = {
      'userId': id
    };
    console.log('user details api query', query);
    this.Detail.getAllUserDetails(query)
      .then(
        (res: any) => {
          console.log(res, 'user details api response');
          this.details = res['data'];
          this.editData = res['data'][0];
          this.dataLoaded = true;
        },
        (err: Object) => {
          console.log(err, 'user details api error');
        }
      )
      .catch((err: Object) => {
        console.log(err, 'user details error catch');
    });
  }
}
