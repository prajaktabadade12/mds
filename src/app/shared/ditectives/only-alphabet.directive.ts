import { Directive, HostListener, ElementRef, Input } from '@angular/core';
@Directive({
  selector: '[onlyAlphabets]'
})
export class OnlyAlphabetsDirective {

  regexStr = '^[a-zA-Z \-\']+';
  @Input() isAlphaNumeric: boolean;

  constructor(private el: ElementRef) { }


  @HostListener('keypress', ['$event']) onKeyPress(event) {
    return new RegExp(this.regexStr).test(event.key);
  }

  @HostListener('paste', ['$event']) blockPaste(event: KeyboardEvent) {
    this.validateFields(event);
  }

  validateFields(event) {
    setTimeout(() => {
       this.el.nativeElement.value = '';
      event.preventDefault();
    }, 100);
  }
}

// only alphabel
