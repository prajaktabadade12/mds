import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  editDetailsForm = new FormGroup({
    name: new FormControl(''),
    contact:  new FormControl(''),
    restaurant: new FormControl('')
  });
  popupModelDetails = {
    name: '',
    contact: '',
    restaurant: ''
  };
  constructor(private router: Router, private snapshot: ActivatedRoute) {
    this.popupModelDetails = {
      name: atob(this.snapshot.snapshot.params.name),
      contact: atob(this.snapshot.snapshot.params.contact),
      restaurant: atob(this.snapshot.snapshot.params.restaurant)
    };
   }

  ngOnInit(): void {
  }
  goBack() {
    this.router.navigate(['/kitchen']);
  }
  Update() {}
}
