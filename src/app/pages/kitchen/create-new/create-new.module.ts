import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateNewRoutingModule } from './create-new-routing.module';
import { CreateNewComponent } from './create-new.component';
import { MatInputModule,  } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CreateNewComponent],
  imports: [
    CommonModule,
    CreateNewRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class CreateNewModule { }
