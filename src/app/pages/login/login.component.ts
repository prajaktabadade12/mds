import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';

import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { CommonDataService } from '../../shared/services/common-data.service';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { timer, Subject, interval, BehaviorSubject, Observable } from 'rxjs';
import { StorageService } from '../../shared/services/storage.service';
import { NotificationService } from '../../shared/services/notification.service';
import Swal from 'sweetalert2';
declare var $: any;


@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {
    login: FormGroup;
    OtpForm: FormGroup;
    subscribeTimer;
    timeLeft = 30;
    pause = new Subject();
    rolls;
    countryCode;
    selectedRole;
    transactionId;
    islogin = false;
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    constructor(private element: ElementRef, private formBuilder: FormBuilder,
      private dataService: CommonDataService, private router: Router,
      private LoginServic: LoginService,
      private storage: StorageService,
      private notify: NotificationService
      ) {
        this.rolls = dataService.loginRoleData();
        this.countryCode = dataService.countryCodeData();
    }

    ngOnInit() {
      this.storage.clear();
      this.login = this.formBuilder.group({
        countryCode: [this.countryCode[0].code, Validators.required],
        mobileNumber: [null, [Validators.required, Validators.minLength(8)]],
      });

      this.OtpForm = this.formBuilder.group({
        loginOtp1: [null, Validators.required],
        loginOtp2: [null, Validators.required],
        loginOtp3: [null, Validators.required],
        loginOtp4: [null, Validators.required],
        loginOtp5: [null, Validators.required],
        loginOtp6: [null, Validators.required],
      });

    }
    onLogin() {
      if (this.login.valid) {
        if (this.selectedRole) {
            this.signIn();
          } else {
            this.notify.showWarning('Warning !', 'Select user roll');
            // Swal.fire({
            //   title: 'Warning!',
            //   text: 'Select user role',
            //   icon: 'warning',
            //   confirmButtonText: 'Ok',
            //   width: '22rem',
            // });
          }
        } else {
          this.validateAllFormFields(this.login);
        }
      }
      validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
          }
        });
      }
    ngOnDestroy() {
    }
    selectRole(role) {
      this.selectedRole = role;
  }

  signIn() {
    const query = {
      'mobileNumber': this.login.value.countryCode + '.' + this.login.value.mobileNumber,
      'userRole': this.selectedRole.replace(' ', '_')
    };
    console.log('login api query', query);
    this.LoginServic.login(query)
      .then(
        (res: any) => {
          if (res['data'][0].newUser) {
            this.notify.showInfo('Please register to log in !!', 'New User');
          } else {
            console.log(res, 'login api response');
            this.islogin = true;
            this.transactionId = res['data'][0].transactionId;
          }
          // this.observableTimer();
        },
        (err: Object) => {
          console.log(err, 'login api error');
        }
      )
      .catch((err: Object) => {
        console.log(err, 'login error catch');
    });
  }

  // move to next input
  movetoNext(current, nextFieldID) {
    if (this.OtpForm.controls[current].value.length >= 1) {
      document.getElementById(nextFieldID).focus();
    }
  }

  // timer
  observableTimer() {
    const source = timer(1000, 2000);
    const abc = source.subscribe(val => {
      // console.log(this.subscribeTimer, ' this.subscribeTimer ');
      if (this.subscribeTimer >= 0) {
        this.subscribeTimer = this.timeLeft - val;
        // console.log(this.subscribeTimer, ' this.subscribeTimer ');
      } else {
        // this.subscribeTimer.stop();
        abc.unsubscribe();
        // console.log(this.subscribeTimer, ' this.subscribeTimer stop');
      }
    });
  }

  // resend otp
  resendOtp() {
    this.OtpForm.reset();
    this.subscribeTimer = 0;
    this.observableTimer();
    const query = {
      'transactionId': this.transactionId
    };
    console.log('resend api query', query);
    this.LoginServic.getResendOtp(query)
      .then(
        (res: any) => {
          this.notify.showInfo('OTP Sent Successfully !!', 'OTP Sent');
          console.log(res, 'resend Otp api response');
          this.transactionId = res['data'][0].transactionId;
        },
        (err: Object) => {
          console.log(err, 'resend Otp api error');
        }
      )
      .catch((err: Object) => {
        console.log(err, 'resend Otp error catch');
    });
  }

  // submit otp
  SubmitOtp() {
    // console.log('token', this.storage.getItem('jwtToken'));
    if (this.OtpForm.valid) {
      console.log(this.OtpForm, 'form');
      const query = {
        'transactionId': this.transactionId,
        'otp': this.OtpForm.value.loginOtp1 + '' + this.OtpForm.value.loginOtp2 + '' + this.OtpForm.value.loginOtp3 + ''
              + this.OtpForm.value.loginOtp4 + '' + this.OtpForm.value.loginOtp5 + '' + this.OtpForm.value.loginOtp6
      };
      console.log('submit api query', query);
      this.LoginServic.getSubmitOtp(query)
        .then(
          (res: any) => {

            if (res['data']) {
              this.storage.setData(res['data'][0]);
              // this.storage.setItem('jwtToken', res.data[0].jwtToken);
              // this.storage.setData(res.data[0].userDetails);
              // this.currentUserSubject.next(res);
              console.log(res, 'submit Otp api response');
              // console.log('jwt token', this.storage.getItem('jwtToken'));
              this.islogin = true;
              this.notify.showSuccess('', 'Login Successfull !!');
              this.router.navigate(['/dashboard']);
            }
            // tslint:disable-next-line:triple-equals
            // if (res.errorResponse.errorCode == 400) {
              // this.notify.showWarning('', 'Invalid OTP');
            // }
          },
          (err: Object) => {
            console.log(err, 'submit Otp api error');
          }
        )
        .catch((err: Object) => {
          console.log(err, 'submit Otp error catch');
      });
    }
  }
}
