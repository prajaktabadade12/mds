import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';


import { createRoutes } from './create-routing.module';
import { UserComponent } from './user/user.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { NgxMaterialTimepickerModule, NgxMaterialTimepickerComponent } from 'ngx-material-timepicker';
import { SharedModule } from '../shared/shared.module';
import { MapModule } from '../shared/map/map.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(createRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
    NgxMaterialTimepickerModule,
    SharedModule,
    MapModule,
    NgbModule
    // MatFormFieldModule,
    // MatInputModule
  ],
  declarations: [
    UserComponent,
    RestaurantComponent,
    // NgxMaterialTimepickerComponent
  ]
})
export class CreateModule { }
