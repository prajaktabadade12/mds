// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit, OnChanges, AfterViewInit, SimpleChanges, Inject } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormBuilder } from '@angular/forms';
import { CommonDataService } from '../../shared/services/common-data.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateService } from '../create.service';
import { MastersService } from '../../masters.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { StorageService } from '../../shared/services/storage.service';
declare const $: any;
interface FileReaderEventTarget extends EventTarget {
    result: string;
}

interface FileReaderEvent extends Event {
    target: EventTarget;
    getMessage(): string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  selectedValue = '+1';
  imageAddedCount = 1;
  paymentMethod = 'Paypal';
  countryList = [];
  paymentArray = [];
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();
  countryCode;
  type: FormGroup;
  isAlwaysOpenMon = false;
  isAlwayCloseMon = false;
  isAlwaysOpentue = false;
  isAlwayClosetue = false;
  isAlwaysOpenwed = false;
  isAlwayClosewed = false;
  isAlwaysOpenthu = false;
  isAlwayClosethu = false;
  isAlwaysOpenfri = false;
  isAlwayClosefri = false;
  isAlwaysOpensat = false;
  isAlwayClosesat = false;
  isAlwaysOpensun = false;
  isAlwayClosesun = false;
  isPickUpAvailable =  true;
  isHomeDeliveryAvailable =  true;
  editDetails = false;
  title = 'Create New Restaurant';
  check = true;
  mondayOpen = '';
  mondayClose = '';
  YuesdayOpen = '';
  YuesdayClose = '';
  wednesdayOpen = '';
  wednesdayClose = '';
  thursdayOpen = '';
  thursdayClose = '';
  fridayOpen = '';
  fridayClose = '';
  saturdayOpen = '';
  saturdayClose = '';
  sundayOpen = '';
  sundayClose = '';
  modalRefrence;
  filepath;
  filename;
  task: AngularFireUploadTask;
  progressValue: Observable<number>;
  downloadableURL;

  idProofUrl;
  ifProoftask: AngularFireUploadTask;
  idproofProgress: Observable<number>;
  idProofPath;
  idProofName;

  addressUrl;
  addresstask: AngularFireUploadTask;
  addressProgress: Observable<number>;
  addressPath;
  addressName;

  otherUrl;
  othertask: AngularFireUploadTask;
  otherProgress: Observable<number>;
  otherPath;
  otherName;
  inde;
  mapSubmitted = false;
  uploadProgress = false;
  uuid: any;
  editData = {
    averageCookingTime: '',
    averageDeliveryTime: '',
    blocked: '',
    covidSafetyLevel: 'HIGH',
    email: '',
    homeDeliveryAvailable: '',
    id: '',
    name: '',
    pickupAvailable: '',
    rating: '',
    restaurantStatus: '',
    serviceTax: '',
    // addressUrl: '',
    // idProofUrl: '',
    // otherUrl: '',
    administrativeArea: '',
    countryCode: '',
    countryName: '',
    landmark: '',
    latitude: '',
    locality: '',
    longitude: '',
    postalCode: '',
    premise: '',
    streetAddress: '',
    subAdministrativeArea: '',
    mon: '',
    monc: '',
    tue: '',
    tuec: '',
    wed: '',
    wedc: '',
    thu: '',
    thuc: '',
    fri: '',
    fric: '',
    sat: '',
    satc: '',
    sun: '',
    sunc: ''
  };
  restoDetails: any;
  constructor(private formBuilder: FormBuilder,
    private dataService: CommonDataService,
    private Service: CreateService,
    private modalService: NgbModal,
    public notify: NotificationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private storage: StorageService,
    @Inject(DOCUMENT) private _document: Document,
    private MasterService: MastersService,
    private angularFire: AngularFireStorage) {
      this.getMasters();
      this.countryCode = dataService.countryCodeData();
    //   console.log(this.countryCode, 'country Code');
      this.paymentMethod = 'Paypal';
    //   console.log(this.storage.getItem('userDetails'));
      this.uuid = this.storage.getItem('userDetails');
      if (activatedRoute.snapshot.params.edit !== undefined) {
        this.editDetails =  activatedRoute.snapshot.params.edit;
        this.title = 'Edit Restaurant';
    } else {
        this.editDetails = false;
    }
  }


  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }
    ngOnInit() {

      const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
      let numberDetails = [];
      let whatsAppNumber = [];

      if (this.editDetails) {
        this.restoDetails = this.storage.getItem('editRestaurantDetails');
        // console.log('data in storage', this.restoDetails);
        numberDetails = this.restoDetails.mobileNumber.split('.');
        whatsAppNumber = this.restoDetails.whatsAppNumber.split('.');
      //   console.log(numberDetails, 'number details');
      this.isPickUpAvailable = this.restoDetails.pickupAvailable;
      this.isHomeDeliveryAvailable = this.restoDetails.homeDeliveryAvailable;
      this.editData = {
        averageCookingTime: this.restoDetails.averageCookingTime,
        averageDeliveryTime: this.restoDetails.averageDeliveryTime,
        blocked: this.restoDetails.blocked,
        covidSafetyLevel: this.restoDetails.covidSafetyLevel,
        email: this.restoDetails.email,
        homeDeliveryAvailable: this.restoDetails.homeDeliveryAvailable,
        id: this.restoDetails.id,
        name: this.restoDetails.name,
        pickupAvailable: this.restoDetails.pickupAvailable,
        rating: this.restoDetails.rating,
        restaurantStatus: this.restoDetails.restaurantStatus,
        // addressUrl: this.restoDetails.documentList[0].documentURL,
        // idProofUrl: this.restoDetails.documentList[1].documentURL,
        // otherUrl: this.restoDetails.documentList[2].documentURL,
        serviceTax: this.restoDetails.serviceTax,
        administrativeArea: this.restoDetails.restaurantAddressList[0].administrativeArea,
        countryCode: this.restoDetails.restaurantAddressList[0].countryCode,
        countryName: this.restoDetails.restaurantAddressList[0].countryName,
        landmark: this.restoDetails.restaurantAddressList[0].landmark,
        latitude: this.restoDetails.restaurantAddressList[0].latitude,
        locality: this.restoDetails.restaurantAddressList[0].locality,
        longitude: this.restoDetails.restaurantAddressList[0].longitude,
        postalCode: this.restoDetails.restaurantAddressList[0].postalCode,
        premise: this.restoDetails.restaurantAddressList[0].premise,
        streetAddress: this.restoDetails.restaurantAddressList[0].streetAddress,
        subAdministrativeArea: this.restoDetails.restaurantAddressList[0].subAdministrativeArea,
        sun: this.restoDetails.restaurantTimings[0].fromTime,
        sunc: this.restoDetails.restaurantTimings[0].toTime,
        mon: this.restoDetails.restaurantTimings[1].fromTime,
        monc: this.restoDetails.restaurantTimings[1].toTime,
        tue: this.restoDetails.restaurantTimings[2].fromTime,
        tuec: this.restoDetails.restaurantTimings[2].toTime,
        wed: this.restoDetails.restaurantTimings[3].fromTime,
        wedc: this.restoDetails.restaurantTimings[3].toTime,
        thu: this.restoDetails.restaurantTimings[4].fromTime,
        thuc: this.restoDetails.restaurantTimings[4].toTime,
        fri: this.restoDetails.restaurantTimings[5].fromTime,
        fric: this.restoDetails.restaurantTimings[5].toTime,
        sat: this.restoDetails.restaurantTimings[6].fromTime,
        satc: this.restoDetails.restaurantTimings[6].toTime,
      };
      this.isAlwaysOpensun = this.restoDetails.restaurantTimings[0]['24HoursOpen'];
      this.isAlwayClosesun = this.restoDetails.restaurantTimings[0]['closedToday'];
      this.isAlwaysOpenMon = this.restoDetails.restaurantTimings[1]['24HoursOpen'];
      this.isAlwayCloseMon = this.restoDetails.restaurantTimings[1]['closedToday'];
      this.isAlwaysOpentue = this.restoDetails.restaurantTimings[2]['24HoursOpen'];
      this.isAlwayClosetue = this.restoDetails.restaurantTimings[2]['closedToday'];
      this.isAlwaysOpenwed = this.restoDetails.restaurantTimings[3]['24HoursOpen'];
      this.isAlwayClosewed = this.restoDetails.restaurantTimings[3]['closedToday'];
      this.isAlwaysOpenthu = this.restoDetails.restaurantTimings[4]['24HoursOpen'];
      this.isAlwayClosethu = this.restoDetails.restaurantTimings[4]['closedToday'];
      this.isAlwaysOpenfri = this.restoDetails.restaurantTimings[5]['24HoursOpen'];
      this.isAlwayClosefri = this.restoDetails.restaurantTimings[5]['closedToday'];
      this.isAlwaysOpensat = this.restoDetails.restaurantTimings[6]['24HoursOpen'];
      this.isAlwayClosesat = this.restoDetails.restaurantTimings[6]['closedToday'];

    }

      this.type = this.formBuilder.group({
        email: [this.editData.email, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        mob_code: [this.countryCode[0].code],
        mob_number: [numberDetails[1], Validators.required],
        whatCode: [this.countryCode[0].code],
        what_number: [whatsAppNumber[1]],

        premise: [this.editData.premise, Validators.required],
        street:  [this.editData.streetAddress, Validators.required],
        locality:  [this.editData.locality, Validators.required],
        sub_administrative:  [this.editData.subAdministrativeArea],
        administrative: [this.editData.administrativeArea, Validators.required],
        postal_code: [this.editData.postalCode, Validators.required],
        landmark:  [this.editData.landmark, Validators.required],
        addressType: ['WORK'],
        countryName: [this.editData.countryName],
        longitude: [this.editData.longitude, Validators.required],
        latitude:  [this.editData.latitude, Validators.required],

        restuarant_name: [this.editData.name, Validators.required],
        service_tax: [this.editData.serviceTax, Validators.required],
        order_range: [null, Validators.required],
        payment_method: ['Paypal', Validators.required],
        images: this.formBuilder.array([]),
        bank_account_details: this.formBuilder.array([this.accountDetalils()]),
        covid_safety: [this.editData.covidSafetyLevel, Validators.required],
        idProofDescription: [null],
        addProofDescription: [null],
        otherDescription: [null],
        mon: [this.editData.mon],
        monc: [this.editData.monc],
        tue: [this.editData.tue],
        tuec: [this.editData.tuec],
        wed: [this.editData.wed],
        wedc: [this.editData.wedc],
        thu: [this.editData.thu],
        thuc: [this.editData.thuc],
        fri: [this.editData.fri],
        fric: [this.editData.fric],
        sat: [this.editData.sat],
        satc: [this.editData.satc],
        sun: [this.editData.sun],
        sunc: [this.editData.sunc]
       });
       if (this.editDetails) {
        this.type.patchValue({
            mobile_code: numberDetails[0],
            What_code: whatsAppNumber[0],
            // idProofDescription: this.restoDetails.documentList[1].description,
            // addProofDescription: this.restoDetails.documentList[0].description,
            // otherDescription: this.restoDetails.documentList[2].description
           });
        if (this.restoDetails.restaurantImagesList.length > 0) {
            const reader = new FileReader();

              for (const item of this.restoDetails.restaurantImagesList) {
                this.formArray.push(this.initImages(item.imageURL, item.imagePath));
                this.inde = this.type.controls.images['controls'].length - 1;
                // this.downloadableURL.push(item.imageURL);
                // console.log(this.type.controls.images['controls'].value.imageURL, 'image controls');

                // reader.onload = function(e) {
                //     console.log(`#restaurantPreview${innd}`, 'id');
                //     $(`#restaurantPreview${innd}`).attr('src', e.target.result);
                // };
                // // reader.readAsDataURL(fileData);
                // reader.readAsText(item.imageURL);
                // reader.
                // this.uploadProgress = false;
              }
          }
       }
        // Code for the Validator
        const $validator = $('.card-wizard form').validate({
            rules: {
                email: {
                    required: true,
                    minlength: 3,
                },
                restuarant_name: {
                    required: true,
                    minlength: 2
                },
                mob_number: {
                    required: true,
                    minlength: 2
                },
                what_number: {
                    required: true,
                    minlength: 2
                },
                service_tax: {
                    required: true,
                    minlength: 1
                },
                order_range: {
                    required: true,
                    minlength: 1
                },
                premise: {
                    required: true,
                    minlength: 2
                },
                street: {
                    required: true,
                    minlength: 2
                },
                locality: {
                    required: true,
                    minlength: 2
                },
                administrative: {
                    required: true,
                    minlength: 2
                },
                landmark: {
                    required: true,
                    minlength: 2
                },
                longitude: {
                    required: true,
                    minlength: 2
                },
                latitude: {
                    required: true,
                    minlength: 2
                },
            },
            highlight: function(element) {
              $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
            },
            success: function(element) {
              $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
            },
            errorPlacement : function(error, element) {
              $(element).append(error);
            }
        });

        // Wizard Initialization
        $('.card-wizard').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'nextSelector': '.btn-next',
            'previousSelector': '.btn-previous',

            onNext: function(tab, navigation, index) {
                var $valid = $('.card-wizard form').valid();
                if(!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            },

            onInit: function(tab: any, navigation: any, index: any) {

              // check number of tabs and fill the entire row
              let $total = navigation.find('li').length;
              let $wizard = navigation.closest('.card-wizard');

              let $first_li = navigation.find('li:first-child a').html();
              let $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
              $('.card-wizard .wizard-navigation').append($moving_div);

              $total = $wizard.find('.nav li').length;
             let  $li_width = 100/$total;

              let total_steps = $wizard.find('.nav li').length;
              let move_distance = $wizard.width() / total_steps;
              let index_temp = index;
              let vertical_level = 0;

              let mobile_device = $(document).width() < 600 && $total > 3;

              if(mobile_device){
                  move_distance = $wizard.width() / 2;
                  index_temp = index % 2;
                  $li_width = 50;
              }

              $wizard.find('.nav li').css('width',$li_width + '%');

              let step_width = move_distance;
              move_distance = move_distance * index_temp;

              let $current = index + 1;

              if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
                  move_distance -= 8;
              } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
                  move_distance += 8;
              }

              if(mobile_device){
                  let x: any = index / 2;
                  vertical_level = parseInt(x);
                  vertical_level = vertical_level * 38;
              }

              $wizard.find('.moving-tab').css('width', step_width);
              $('.moving-tab').css({
                  'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
                  'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

              });
              $('.moving-tab').css('transition', 'transform 0s');
           },

            onTabClick : function(tab: any, navigation: any, index: any) {

                const $valid = $('.card-wizard form').valid();

                if (!$valid) {
                    return false;
                } else {
                    return true;
                }
            },

            onTabShow: function(tab: any, navigation: any, index: any) {
                let $total = navigation.find('li').length;
                let $current = index + 1;
                if (elemMainPanel) {
                  elemMainPanel.scrollTop = 0;
                }
                // console.log(elemMainPanel, 'element panel');
                // elemMainPanel.scrollTop = 0;
                const $wizard = navigation.closest('.card-wizard');

                // If it's the last tab then hide the last button and show the finish instead
                if ($current >= $total) {
                    $($wizard).find('.btn-next').hide();
                    $($wizard).find('.btn-finish').show();
                } else {
                    $($wizard).find('.btn-next').show();
                    $($wizard).find('.btn-finish').hide();
                }

                const button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                setTimeout(function() {
                    $('.moving-tab').text(button_text);
                }, 150);

                const checkbox = $('.footer-checkbox');

                if ( index !== 0 ) {
                    $(checkbox).css({
                        'opacity':'0',
                        'visibility':'hidden',
                        'position':'absolute'
                    });
                } else {
                    $(checkbox).css({
                        'opacity':'1',
                        'visibility':'visible'
                    });
                }
                $total = $wizard.find('.nav li').length;
               let  $li_width = 100/$total;

                let total_steps = $wizard.find('.nav li').length;
                let move_distance = $wizard.width() / total_steps;
                let index_temp = index;
                let vertical_level = 0;

                let mobile_device = $(document).width() < 600 && $total > 3;

                if(mobile_device){
                    move_distance = $wizard.width() / 2;
                    index_temp = index % 2;
                    $li_width = 50;
                }

                $wizard.find('.nav li').css('width',$li_width + '%');

                let step_width = move_distance;
                move_distance = move_distance * index_temp;

                $current = index + 1;

                if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
                    move_distance -= 8;
                } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
                    move_distance += 8;
                }

                if(mobile_device){
                    let x: any = index / 2;
                    vertical_level = parseInt(x);
                    vertical_level = vertical_level * 38;
                }

                $wizard.find('.moving-tab').css('width', step_width);
                $('.moving-tab').css({
                    'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
                    'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

                });
            }
        });


        // Prepare the preview for profile picture
        $('#wizard-picture').change(function() {
            const input = $(this);

            if (input[0].files && input[0].files[0]) {
                const reader = new FileReader();

                reader.onload = function (e: any) {
                    $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                };
                reader.readAsDataURL(input[0].files[0]);
            }
        });
        $('[data-toggle="wizard-radio"]').click(function() {
            const wizard = $(this).closest('.card-wizard');
            wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
            $(this).addClass('active');
            $(wizard).find('[type="radio"]').removeAttr('checked');
            $(this).find('[type="radio"]').attr('checked', 'true');
        });

        $('[data-toggle="wizard-checkbox"]').click(function(){
            if ( $(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('[type="checkbox"]').removeAttr('checked');
            } else {
                $(this).addClass('active');
                $(this).find('[type="checkbox"]').attr('checked', 'true');
            }
        });

        $('.set-full-height').css('height', 'auto');

    }
    ngOnChanges(changes: SimpleChanges) {
        const input = $(this);

        if (input[0].files && input[0].files[0]) {
            const reader: any = new FileReader();

            reader.onload = function (e: any) {
                $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
            };
            reader.readAsDataURL(input[0].files[0]);
        }
    }
    ngAfterViewInit() {

        $( window ).resize( () => { $('.card-wizard').each(function(){
          setTimeout(() => {
            const $wizard = $(this);
            const index = $wizard.bootstrapWizard('currentIndex');
            let $total = $wizard.find('.nav li').length;
            let  $li_width = 100/$total;

            let total_steps = $wizard.find('.nav li').length;
            let move_distance = $wizard.width() / total_steps;
            let index_temp = index;
            let vertical_level = 0;

            let mobile_device = $(document).width() < 600 && $total > 3;
            if(mobile_device){
                move_distance = $wizard.width() / 2;
                index_temp = index % 2;
                $li_width = 50;
            }

            $wizard.find('.nav li').css('width',$li_width + '%');

            let step_width = move_distance;
            move_distance = move_distance * index_temp;

            let $current = index + 1;

            if($current == 1 || (mobile_device == true && (index % 2 == 0) )){
                move_distance -= 8;
            } else if($current == total_steps || (mobile_device == true && (index % 2 == 1))){
                move_distance += 8;
            }

            if(mobile_device){
                let x: any = index / 2;
                vertical_level = parseInt(x);
                vertical_level = vertical_level * 38;
            }

            $wizard.find('.moving-tab').css('width', step_width);
            $('.moving-tab').css({
                'transform':'translate3d(' + move_distance + 'px, ' + vertical_level +  'px, 0)',
                'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'
            });

            $('.moving-tab').css({
                'transition': 'transform 0s'
            });
          }, 500);

        });
    });

  }

    // initial fields for form
    initImages(url, path) {
        return this.formBuilder.group({
            imageURL: url,
            imagePath: path,
        });
    }

    // load image
    LoadFile(file) {
        // console.log('file');
        const fileData = file.target.files[0];
        if (fileData) {
            this.upload(fileData);
        }
    }
    async upload(fileData) {
        this.uploadProgress = true;
        this.inde = this.type.controls.images['controls'].length - 1;
        this.filepath =  Math.random() + fileData;
        this.task = this.angularFire.upload(`restaurant/image/${this.filepath}`, fileData);
        this.progressValue = this.task.percentageChanges();
        (await this.task).ref.getDownloadURL().then(url => { this.downloadableURL = url;
            this.formArray.push(this.initImages(url, `restaurant/image/${this.filepath}`));
            // const reader = new FileReader();
            // const innd = this.type.controls.images['controls'].length - 1;
            // console.log(this.inde, 'index');
            // reader.onload = function(e) {
            //     console.log(`#restaurantPreview${innd}`, 'id');
            //     $(`#restaurantPreview${innd}`).attr('src', e.target.result);
            // };
            // reader.readAsDataURL(fileData);
            this.uploadProgress = false;
        }, (err) => {
            this.uploadProgress = false;
        });

    }
    // load id proof
    loadIdProof(file) {
        const fileData = file.target.files[0];
        if (fileData) {
            const reader = new FileReader();
            reader.onload = function(e) {
                $(`#idProofPreview`).attr('src', e.target.result);
            }
        }
    }
    loadAddressProof(file) {
        const fileData = file.target.files[0];
        if (fileData) {
            const reader = new FileReader();
            reader.onload = function(e) {
                $(`#addreddPreview`).attr('src', e.target.result);
            }
        }
    }

    loadOthers(file) {
        const fileData = file.target.files[0];
        if (fileData) {
            const reader = new FileReader();
            reader.onload = function(e) {
                $(`#otherPreview`).attr('src', e.target.result);
            }
        }
    }
      // add more image
    addMoreImages() {
        // console.log('getting in');
        document.getElementById('restaurant').click();
        this.imageAddedCount = this.imageAddedCount + 1;
    }

    // remove images
    removeImages(index) {
        const len = this.type.controls.images['controls'].length - 1;
        this.formArray.removeAt(index);
    }

    get formArray() {
        return this.type.get('images') as FormArray
    }
    isHomeDelivery() {
        this.isHomeDeliveryAvailable = ! this.isHomeDeliveryAvailable;
    }

    pickupAvailable() {
        this.isPickUpAvailable = ! this.isPickUpAvailable;
    }
    accountDetalils() {
        return this.formBuilder.group({
            accountHolderName: [''],
            accountNum: [''],
            bankId: [''],
            branchId: [''],
            iban: [''],
            swiftBic: [''],
            country: [''],
            currencyCode: [''],
            receiptId: [''],
            primaryMethod: [''],
            emailAddress: [''],
            paymentMethod: ['BANK']
        });
    }

    // add more image
    AddmoreBank() {
        this.bankDetails.push(this.accountDetalils());
    }

    // remove images
    removeBank(index) {
        this.bankDetails.removeAt(index);
    }

    get bankDetails() {
        return this.type.get('bank_account_details') as FormArray
    }
    openMap(map) {
        this.mapSubmitted = false;
        this.openModal(map);
    }
    submitMapLoaction () {
        this.mapSubmitted = true;
        setTimeout(()  => {
            this.modalRefrence.close();
        }, 900);
        // this.modalService.dismiss('Cross click');
    }
    openModal(map) {
        this.modalRefrence = this.modalService.open(map, {
        backdrop: true,
        centered: true,
        keyboard: true,
        });
    }
    getDismissReason(reason: any) {
        throw new Error('Method not implemented.');
    }

    selectedPayment(method) {
        this.paymentMethod = method;
    }

    createRestaurant() {
        // console.log(this.type, 'formdata');
          const query = {
            name: this.type.value.restuarant_name,
            maximumKmAllowedToOnlineOrder: this.type.value.order_range,
            mobileNumber: this.type.value.mob_code + '.' + this.type.value.mob_number,
            whatsAppNumber: this.type.value.whatCode + '.' + this.type.value.what_number,
            email: this.type.value.email,
            createdBy: this.uuid.id,
            serviceTax: this.type.value.service_tax,
            covidSafetyLevel: this.type.value.covid_safety,
            restaurantAddressList: [{
                countryCode: this.type.value.mob_code,
                countryName: this.type.value.countryName,
                administrativeArea: this.type.value.administrative,
                subAdministrativeArea: this.type.value.sub_administrative,
                locality: this.type.value.locality,
                streetAddress: this.type.value.street,
                premise: this.type.value.premise,
                landmark: this.type.value.landmark,
                postalCode: this.type.value.postal_code,
                longitude: this.type.value.longitude,
                latitude: this.type.value.latitude
                }],
            restaurantTimings: [
              {
                dayOfWeek: 'SUN',
                fromTime: this.type.value.sun,
                toTime: this.type.value.sunc,
                is24HoursOpen: this.isAlwaysOpensun,
                closedToday: this.isAlwayClosesun
              },
              {
                dayOfWeek: 'MON',
                fromTime: this.type.value.mon,
                toTime: this.type.value.monc,
                is24HoursOpen: this.isAlwaysOpenMon,
                closedToday: this.isAlwayCloseMon
              },
              {
                dayOfWeek: 'TUE',
                fromTime: this.type.value.tue,
                toTime: this.type.value.tuec,
                is24HoursOpen: this.isAlwaysOpentue,
                closedToday: this.isAlwayClosetue
              },
              {
                dayOfWeek: 'WED',
                fromTime: this.type.value.wed,
                toTime: this.type.value.wedc,
                is24HoursOpen: this.isAlwaysOpenwed,
                closedToday: this.isAlwayClosewed
              },
              {
                dayOfWeek: 'THU',
                fromTime: this.type.value.thu,
                toTime: this.type.value.thuc,
                is24HoursOpen: this.isAlwaysOpenthu,
                closedToday: this.isAlwayClosethu
              },
              {
                dayOfWeek: 'FRI',
                fromTime: this.type.value.fri,
                toTime: this.type.value.fric,
                is24HoursOpen: this.isAlwaysOpenfri,
                closedToday: this.isAlwayClosefri
              },
              {
                dayOfWeek: 'SAT',
                fromTime: this.type.value.sat,
                toTime: this.type.value.satc,
                is24HoursOpen: this.isAlwaysOpensat,
                closedToday: this.isAlwayClosesat
              }
            ],
            restaurantImagesList: this.formArray.value,
            restaurantDocumentList: [
                {
                    documentType: 'ID_PROOF',
                    documentURL:  `${this.idProofUrl}`,
                    documentPath: `restaurant/Document/${this.idProofPath}`,
                    description: this.type.value.idProofDescription
                },
                {
                    documentType: 'ADDRESS_PROOF',
                    documentURL:  `${this.addressUrl}`,
                    documentPath: `restaurant/Document/${this.addressPath}`,
                    description: this.type.value.addProofDescription
                },
                {
                    documentType: 'OTHER',
                    documentURL: `${this.otherUrl}`,
                    documentPath: `restaurant/Document/${this.otherPath}`,
                    description: this.type.value.otherDescription
                }
                ],
            restaurantPayoutMethodDetailsList: this.bankDetails.value,
            homeDeliveryAvailable: this.isHomeDeliveryAvailable,
            pickupAvailable: this.isPickUpAvailable
            };
        // console.log('create restaurant api query', query);
        this.Service.createRestaurant(query)
            .then(
                (res: any) => {
                    // console.log(res, 'create api response');
                    this.notify.showSuccess('Restaurant created succcessfully.', 'Succesfully Registered');
                    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                    this.router.onSameUrlNavigation = 'reload';
                    this.router.navigate(['create/restaurant']);
                },
                (err: Object) => {
                    this.notify.showError(err['error'].errorResponse.description, 'Error !!!');
                    this.router.navigate([this.router.url]);
                    // console.log(err, 'create api error');
                }
            )
            .catch((err: Object) => {
                // console.log(err, 'create api error catch');
        });
    }
    monOpen() {
        this.isAlwaysOpenMon = ! this.isAlwaysOpenMon;
        if (this.isAlwaysOpenMon) {
            this.mondayOpen = '';
            this.mondayClose = '';
            this.isAlwayCloseMon = false;
        }
    }

    monClose() {
        this.isAlwayCloseMon = ! this.isAlwayCloseMon;
        if (this.isAlwayCloseMon) {
            this.mondayOpen = '';
            this.mondayClose = '';
            this.isAlwaysOpenMon = false;
        }
    }
    tueOpen() {
        this.isAlwaysOpentue = ! this.isAlwaysOpentue;
        if (this.isAlwaysOpentue) {
            this.YuesdayOpen = '';
            this.YuesdayClose = '';
            this.isAlwayClosetue = false;
        }
    }
    tueClose() {
        this.isAlwayClosetue = ! this.isAlwayClosetue;
        if (this.isAlwayClosetue) {
            this.YuesdayOpen = '';
            this.YuesdayClose = '';
            this.isAlwaysOpentue = false;
        }
    }
    wedOpen() {
        this.isAlwaysOpenwed = ! this.isAlwaysOpenwed;
        if (this.isAlwaysOpenwed) {
            this.wednesdayOpen = '';
            this.wednesdayClose = '';
            this.isAlwayClosewed = false;
        }
    }
    wedclose() {
        this.isAlwayClosewed = ! this.isAlwayClosewed;
        if (this.isAlwayClosewed) {
            this.wednesdayOpen = '';
            this.wednesdayClose = '';
            this.isAlwaysOpenwed = false;
        }

    }
    thuOpen() {
        this.isAlwaysOpenthu = ! this.isAlwaysOpenthu;
        if (this.isAlwaysOpenthu) {
            this.thursdayOpen = '';
            this.thursdayClose = '';
            this.isAlwayClosethu = false;
        }
    }
    thuclose() {
        this.isAlwayClosethu = ! this.isAlwayClosethu;
        if (this.isAlwayClosethu) {
            this.thursdayOpen = '';
            this.thursdayClose = '';
            this.isAlwaysOpenthu = false;
        }

    }
    friOpen() {
        this.isAlwaysOpenfri = ! this.isAlwaysOpenfri;
        if (this.isAlwaysOpenfri) {
            this.fridayOpen = '';
            this.fridayClose = '';
            this.isAlwayClosefri = false;
        }

    }
    friclose() {
        this.isAlwayClosefri = ! this.isAlwayClosefri;
        if (this.isAlwayClosefri) {
            this.fridayOpen = '';
            this.fridayClose = '';
            this.isAlwaysOpenfri = false;
        }
    }
    satOpen() {
        this.isAlwaysOpensat = ! this.isAlwaysOpensat;
        if (this.isAlwaysOpensat) {
            this.saturdayOpen = '';
            this.saturdayClose = '';
            this.isAlwayClosesat = false;
        }

    }
    satclose() {
        this.isAlwayClosesat = ! this.isAlwayClosesat;
        if (this.isAlwayClosesat) {
            this.saturdayOpen = '';
            this.saturdayClose = '';
            this.isAlwaysOpensat = false;
        }
    }
    sunOpen() {
        this.isAlwaysOpensun = ! this.isAlwaysOpensun;
        if (this.isAlwaysOpensun) {
            this.sundayOpen = '';
            this.sundayClose = '';
            this.isAlwayClosesun = false;
        }
    }
    sunclose() {
        this.isAlwayClosesun = ! this.isAlwayClosesun;
        if (this.isAlwayClosesun) {
            this.sundayOpen = '';
            this.sundayClose = '';
            this.isAlwaysOpensun = false;
        }
    }

    getMasters() {
        const query = { };
        // console.log('country api query', query);
        this.MasterService.countryList(query)
            .then(
                (res: any) => {
                    this.countryList = res['data'];
                    // console.log(this.countryList, 'country api response');
                },
                (err: Object) => {
                    // console.log(err, 'country api error');
                }
            )
            .catch((err: Object) => {
                // console.log(err, 'country api error catch');
        });
    }


    mapLocation(event) {
        // console.log('location in components', event);
        let countrName = '';
        let administrativeArea = '';
        let subAdministrativeArea = '';
        let locality = '';
        let streetAddress = '';
        let postalCode = '';
        event[0].address.map((item) => {
            if (item.types[0] === 'country') {
                // console.log('country');
                countrName = item.long_name;
            }
            if (item.types[0] === 'administrative_area_level_1') {
                administrativeArea = item.long_name;
            }
            if (item.types[0] === 'administrative_area_level_2') {
                subAdministrativeArea = item.long_name;
            }
            if (item.types[0] === 'locality') {
                locality = item.long_name;
            }
            if (item.types[0] === 'route') {
                streetAddress = item.long_name;
            }
            if (item.types[0] === 'postal_code') {
                postalCode = item.long_name;
            }
        });
        this.type.patchValue({
            longitude: event[0].longitude,
            latitude:  event[0].latitude,
            countryName: countrName,
            administrative: administrativeArea,
            sub_administrative: subAdministrativeArea,
            locality: locality,
            street: streetAddress,
            postal_code: postalCode,
            premise: '',
            landmark: ''
        });
    }

    async idProofUpload(file) {
        const fileData = file.target.files[0];
        this.idProofName = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.idProofPath =  Math.random() + fileData;
        this.ifProoftask = this.angularFire.upload(`restaurant/Document/${this.idProofPath}`, fileData);
        this.idproofProgress = this.ifProoftask.percentageChanges();
        (await this.ifProoftask).ref.getDownloadURL().then(url => {this.idProofUrl = url; });
        // console.log(this.idProofPath, 'filepath');
        } else {
        alert('No images selected');
        this.idProofUrl = ''; }
    }
    async addressUpload(file) {
        const fileData = file.target.files[0];
        this.addressName = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.addressPath =  Math.random() + fileData;
        this.addresstask = this.angularFire.upload(`restaurant/Document/${this.addressPath}`, fileData);
        this.addressProgress = this.addresstask.percentageChanges();
        (await this.addresstask).ref.getDownloadURL().then(url => {this.addressUrl = url; });
        // console.log(this.addressPath, 'filepath');
        } else {
        alert('No images selected');
        this.addressUrl = ''; }
    }
    async otherUpload(file) {
        const fileData = file.target.files[0];
        this.otherName = file.target.files[0].name;
        // console.log('file', fileData);
        if (fileData) {
        this.otherPath =  Math.random() + fileData;
        this.othertask = this.angularFire.upload(`restaurant/Document/${this.otherPath}`, fileData);
        this.otherProgress = this.othertask.percentageChanges();
        (await this.othertask).ref.getDownloadURL().then(url => {this.otherUrl = url; });
        // console.log(this.otherPath, 'filepath');
        } else {
        alert('No images selected');
        this.otherUrl = ''; }
    }
}

