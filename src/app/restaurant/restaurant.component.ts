import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { GetListService } from '../get-list.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StorageService } from 'src/app/shared/services/storage.service';

export interface List {
  averageCookingTime: number;
  averageDeliveryTime: number;
  blocked: Boolean;
  covidSafetyLevel: String;
  email: String;
  id: String;
  mobileNumber: String;
  name: String;
  rating: number;
  restaurantAddressList: String;
  restaurantImagesList: String;
  restaurantStatus: String;
  restaurantTimings: [];
  whatsAppNumber: String;
}
declare const $: any;

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pageEvent: PageEvent;
  typeRestaurant = '';
  page = 0;
  size = 6;
  modalDetails: any;
  public pageLength;
  public showLoader = false;
  public order = { SUN: 1, MON: 2, TUE: 3, WED: 4, THU: 5, FRI: 6, SAT: 7 };
  public dataSource = new MatTableDataSource<List>();

  constructor(private restaurantList: GetListService,
    private Route: Router,
    private storage: StorageService,
    private modalService: NgbModal) {}

  ngOnInit() {
    this.getList(this.page, this.size);
    this.dataSource.paginator = this.paginator;
  }

  createRestaurant() {
    this.Route.navigate(['/create/restaurant']);
  }

  edit() {
    console.log('edit clicked');
    this.storage.setItem('editRestaurantDetails', this.modalDetails);
    this.Route.navigate(['/create/restaurant',   {edit: true}]);
  }

  ngAfterViewInit() {
    setTimeout(() => {

    });
  }

  getdata(data) {
    return new MatTableDataSource<any>(data);
  }
  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  onPaginate(event: PageEvent) {
    this.page = event.pageIndex;
    this.size = event.pageSize;

    // this.page = this.page + 1;
    this.getList(this.page, this.size);
  }

  searchRestaurant() {
    this.getList(this.page, this.size);
  }
  getList(page, size) {
    this.showLoader = true;
    const query = {
      'searchText': this.typeRestaurant,
      'page': page,
      'size': size,
      'sortBy': 'name',
      'orderBy': 'ASC'
    };
    console.log('list api query', query);
    this.restaurantList.restaurantList(query)
      .then(
          (res: any) => {
            console.log(res, 'restaurant api response');
            this.dataSource.data = res['data'][0].restaurantList as List[];
            this.pageLength = res['data'][0].totalPages * size;
            this.sortArrays();
            setTimeout(() => {
            this.showLoader = false;
            }, 3000);
          },
          (err: Object) => {
            console.log(err, 'list api error');
            setTimeout(() => {
              this.showLoader = false;
              }, 2000);
          }
      )
      .catch((err: Object) => {
        console.log(err, 'list error catch');
        this.showLoader = false;
    });
  }

  sortArrays() {
    this.dataSource.data.map((item) => {
      item.restaurantTimings.sort( (a: any, b: any) => {
        return this.order[a.dayOfWeek] - this.order[b.dayOfWeek];
      });
    });
  }
  restaurantDetails(content, values) {
    this.modalDetails = values;
    console.log('values', this.modalDetails);
    this.openModal(content);
  }
  openModal(content) {
    this.modalService.open(content, {
    backdrop: true,
    centered: true,
    keyboard: true,
    });
  }
}
