export class URLConstants {
    // url
    public static mainUrl = 'http://www.mdssmartorder.com:8080/';


    // login
    public static getlogin = 'login/sign-in';
    public static resendOtp = 'login/resend-otp';
    public static submitOtp = 'login/submit-otp';

    // create url
    public static createUser = 'login/sign-up';
    public static createRestaurant = 'restaurant/create-restaurant';
    public static createFoodCategory = 'restaurant/create-food-category';
    public static createFoodDish = 'restaurant/create-food-dish';
    public static createPromoCode = 'restaurant/create-update-promo-code';


   // list url
    public static getuserDetails = 'user/get-user-list-by-role';
    public static getRestaurantList = 'restaurant/get-restaurant-list';
    public static getFoodCategoryList = 'restaurant/get-food-category-list';
    public static getFoodDishList = 'restaurant/get-food-dish-list';
    public static getPromoCodeList = 'restaurant/get-promo-code-list';

   // Masters URL
   public static countryList = 'utils/get-country-list';
   public static restaurantName = 'restaurant/get-restaurant-name-list';

   // details api
   public static getDetailsUser = 'user/get-user-details';

   // message Token Api
   public static messageToken = 'user/notification/add-notification-token';

}
