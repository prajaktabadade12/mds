import { Injectable } from '@angular/core';
import { HttpService } from './shared/services/http.service';
import { URLConstants } from './urlConstants';
@Injectable({
  providedIn: 'root'
})
export class MastersService {

  constructor(private http: HttpService) { }

  countryList(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.countryList, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  getAllUserDetails(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getDetailsUser, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  storeToken(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.messageToken, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }
}
