import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin/admin.component';
import {CashierComponent} from './cashier/cashier.component';
import {DeliveryBoyComponent} from './delivery-boy/delivery-boy.component';
import {KitchenComponent} from './kitchen/kitchen.component';
import {ManagerComponent} from './manager/manager.component';
import {GlobalAdminComponent} from './global-admin/global-admin.component'
import {SupportComponent} from './support/support.component'

export const UserRoutes: Routes = [
  {
    path: 'admin',
    children: [{
      path: '',
      component: AdminComponent
  }
]},
  {
    path: 'cashier',
    children: [{
      path: '',
      component: CashierComponent
  }
  ]},
  {
    path: 'delivery-boy',
    children: [{
      path: '',
      component: DeliveryBoyComponent
  }
  ]},
  {
    path: 'kitchen',
    children: [{
      path: '',
      component: KitchenComponent
  }
  ]},
  {
    path: 'manager',
    children: [{
      path: '',
      component: ManagerComponent
  }
  ]},
  {
    path: 'global-admin',
    children: [{
      path: '',
      component: GlobalAdminComponent
  }
  ]},
  {
    path: 'support',
    children: [{
      path: '',
      component: SupportComponent
  }
  ]}
];

