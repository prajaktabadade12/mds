import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateFoodCategoryComponent } from './create-food-category/create-food-category.component';
import { ListFoodCategoryComponent } from './list-food-category/list-food-category.component';
export const FoodRoutes: Routes = [
  {
    path: 'create-category',
    children: [{
      path: '',
      component: CreateFoodCategoryComponent
  }
]},
  {
    path: '',
    children: [{
      path: '',
      component: ListFoodCategoryComponent
  }
  ]}
];
