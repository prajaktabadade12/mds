import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { FoodCategoryService } from '../food-category.service';
import { debounceTime, distinctUntilChanged, map, filter, catchError, tap,  switchMap} from 'rxjs/operators';
import { Observable, Subject, merge, of} from 'rxjs';
import { GetListService } from '../../get-list.service';
import { NotificationService } from '../../shared/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { StorageService } from '../../shared/services/storage.service';

interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: EventTarget;
  getMessage(): string;
}

@Component({
  selector: 'app-create-food-category',
  templateUrl: './create-food-category.component.html',
  styleUrls: ['./create-food-category.component.css']
})
export class CreateFoodCategoryComponent implements OnInit {
  filepath;
  downloadableURL = '';
  restaurantSelected = '';
  task: AngularFireUploadTask;
  public model: any;
  progressValue: Observable<number>;
  focusConstitution$ = new Subject<string>();
  clickContitution$  = new Subject<string>();
  restaurant = [];
  filename;
  uuid: any;
  register: FormGroup;
  editData: any = {
    description: '',
    foodDishesList: '',
    id: '',
    imagePath: '',
    imageURL: '',
    name: '',
    restaurantId: '',
  };
  categoryDetails: any;
  editDetails = false;
  constructor(private formBuilder: FormBuilder,
    private Service: FoodCategoryService,
    public notify: NotificationService,
    private activatedRoute: ActivatedRoute,
    private Route: Router,
    private storage: StorageService,
    private List: GetListService,
    private angularFire: AngularFireStorage) {
      if (activatedRoute.snapshot.params.edit !== undefined) {
        this.editDetails =  activatedRoute.snapshot.params.edit;
        this.categoryDetails = this.storage.getItem('editFoodCategory');
        this.editData = {
          description: this.categoryDetails.description,
          foodDishesList: this.categoryDetails.foodDishesList,
          id: this.categoryDetails.id,
          name: this.categoryDetails.name,
          restaurantId: this.categoryDetails.restaurantId,
        };
        this.downloadableURL = this.categoryDetails.imageURL;
      } else {
        this.editDetails = false;
      }
    // console.log(this.storage.getItem('userDetails'));
    this.uuid = this.storage.getItem('userDetails');
  }

  ngOnInit(): void {
    this.restaurantList();
    // console.log('food category');
    this.register = this.formBuilder.group({
      food_category_name: [this.editData.name, Validators.required],
      food_category_description: [this.editData.description],
      restaurant_name: [this.editData.restaurantId, Validators.required],
    });
  }

  async imageUpload(file) {
    const fileData = file.target.files[0];
    this.filename = file.target.files[0].name;
    // console.log('file', fileData);
    if (fileData) {
      this.filepath =  Math.random() + fileData;
      this.task = this.angularFire.upload(`restaurant/food-categories/${this.filepath}`, fileData);
      this.progressValue = this.task.percentageChanges();
      (await this.task).ref.getDownloadURL().then(url => {this.downloadableURL = url; });
      // console.log(this.filepath, 'filepath');
    } else {
      alert('No images selected');
    }
  }

  formatterConstitution(result: any) {
    return result.name;
  }
  selectId(event) {
    // console.log(event);
  }
  searchConstituon = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const inputFocus$ = this.focusConstitution$;
    return merge(debouncedText$, inputFocus$).pipe(
    map(term => (term.length < 0 ? []
    : this.restaurant.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  CreateFoodCategory() {
   const query = {
    'name': this.register.value.food_category_name,
    'description': this.register.value.food_category_description,
    'imageURL': `${this.downloadableURL}`,
    'imagePath': `restaurant/food-categories/${this.filename}`,
    'restaurantId': this.register.value.restaurant_name ? this.register.value.restaurant_name.id : '',
    'createdBy': this.uuid.id
   };
  //  console.log('create food category api query', query);
   this.Service.createFoodCategory(query)
     .then(
      (res: any) => {
        // console.log(res, 'create food category api response');
        this.notify.showSuccess(`${this.register.value.food_category_name} has been created`, 'Succesfully Created');
        setTimeout(() => {
        this.Route.navigate(['/food-category']);
        }, 1000);
      },
      (err: Object) => {
        // console.log(err, 'create food category api error');
        this.notify.showError(err['error'].errorResponse.description,'Error !!!');
        this.Route.navigate([this.Route.url]);
      }
     )
     .catch((err: Object) => {
      // console.log(err, 'create food category error catch');
   });
 }


 restaurantList() {
  const query = {
    'searchText': ''
  };
  // console.log('restaurant Name api query', query);
  this.List.restaurantNameList(query)
    .then(
      (res: any) => {
        // console.log(res, 'restaurant Name api response');
        this.restaurant = res['data'];
        // console.log(this.restaurant , 'restaurant list');
        this.restaurant.map((item) => {
          // tslint:disable-next-line:triple-equals
          // console.log(this.editData.restaurantId);
          // tslint:disable-next-line:triple-equals
          if (item.id == this.editData.restaurantId) {
            // console.log(item.name, 'resto !!!!!!!!!!!!!!!!!!!!!!!!!!');
            this.restaurantSelected = item.name;
          }
        });
      },
      (err: Object) => {
        // console.log(err, 'restaurant Name api error');
      }
    )
    .catch((err: Object) => {
      // console.log(err, 'restaurant Name error catch');
    });
  }

}
