import { Injectable } from '@angular/core';
import { HttpService } from './shared/services/http.service';
import { URLConstants } from './urlConstants';
@Injectable({
  providedIn: 'root'
})
export class GetListService {

  constructor(private http: HttpService) { }

  list(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getuserDetails, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  restaurantList(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.getRestaurantList, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

  restaurantNameList(data) {
    return new Promise((resolve, reject) => {
      this.http.call(data, URLConstants.restaurantName, 'POST').subscribe(
        res => resolve(res),
        err => reject(err)
      );
    });
  }

}
